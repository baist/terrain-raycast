#ifndef BUFFER_HPP
#define BUFFER_HPP

#include <cstddef>

class Buffer
{
public:
    explicit Buffer(size_t size);
    virtual ~Buffer();
    size_t size() const;
    virtual void * data();
    virtual const void * constData() const;
protected:
    size_t m_size;
};

#endif // BUFFER_HPP
