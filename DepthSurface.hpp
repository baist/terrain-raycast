#ifndef DEPTHBUFFER_HPP
#define DEPTHBUFFER_HPP

#include <QVector>

#include "TSurface.hpp"

class DepthSurface: public TSurface<float>
{
public:
    explicit DepthSurface(unsigned int w, unsigned int h);
    virtual ~DepthSurface();
    void clear(float val = 1.0f);
private:
};

#endif // DEPTHBUFFER_HPP
