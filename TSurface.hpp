#ifndef LOCALSURFACE_HPP
#define LOCALSURFACE_HPP

#include <vector>
#include "ColorVal.hpp"
#include "Surface.hpp"

template<class T>
class TSurface: public Surface
{
public:
    TSurface()
    {
    }
    TSurface(unsigned int w, unsigned int h):
        Surface(w, h, sizeof(T))
    {
        m_data.resize(size());
    }
    virtual ~TSurface()
    {
    }
    void * data() override
    {
        return m_data.data();
    }
    virtual const void * constData() const override
    {
        return m_data.data();
    }
    void fill(T val)
    {
        const unsigned int W = width();
        const unsigned int H = height();
        T * p_data = reinterpret_cast<T *>(data());
        std::fill(p_data, p_data + W * H, val);
    }
    T getFrag(unsigned int x, unsigned int y) const
    {
        const size_t off = x + y * width();
        return m_data[off];
    }
private:
    std::vector<T> m_data;
};

#endif // LOCALSURFACE_HPP
