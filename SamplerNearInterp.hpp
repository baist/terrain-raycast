#ifndef SAMPLERNEARINTERP_HPP
#define SAMPLERNEARINTERP_HPP

#include <cmath>
#include "Point2D.hpp"
#include "TSurface.hpp"

template<class T>
class SamplerNearInterp
{
public:
    static Point2Df wrapCoord(const Point2Df & p)
    {
        float intX;
        float fracX = std::modf(p.x, &intX);
        if(std::signbit(p.x)) {
            intX = 1.0f + fracX;
        }
        float intY;
        float fracY = std::modf(p.y, &intY);
        if(std::signbit(p.y)) {
            intY = 1.0f + fracY;
        }
        return Point2Df(fracX, fracY);
    }
public:
    SamplerNearInterp(const TSurface<T> & surf):
        m_surf(surf)
    {

    }

    ~SamplerNearInterp()
    {

    }

    T getFrag(const Point2Df & p) const
    {
        Point2Df newP = wrapCoord(p);
        newP.x *= static_cast<float>(m_surf.width());
        newP.y *= static_cast<float>(m_surf.height());
        const int x0 = std::floor(newP.x);
        const int y0 = std::floor(newP.y);
        return m_surf.getFrag(x0, y0);
    }
private:
    const TSurface<T> & m_surf;
};

#endif // SAMPLERNEARINTERP_HPP
