#ifndef POINT2D_HPP
#define POINT2D_HPP

template<class T>
class Point2D
{
public:
    T x;
    T y;
public:
    Point2D()
    {

    }
    explicit Point2D(T _x, T _y):
        x(_x),
        y(_y)
    {

    }
    ~Point2D()
    {

    }

    Point2D<T> operator + (const Point2D & vec) const
    {
        return Point2D(x + vec.x, y + vec.y);
    }

    Point2D<T> operator - (const Point2D & vec) const
    {
        return Point2D(x - vec.x, y - vec.y);
    }

    Point2D<T> operator * (T scalar) const
    {
        return Point2D(x * scalar, y * scalar);
    }

    Point2D<T> operator * (const Point2D & vec) const
    {
        return Point2D(x * vec.x, y * vec.y);
    }

    Point2D<T> operator / (T scalar) const
    {
        // TODO: handle error scalar=0
        T invScalar = 1.0 / scalar;
        return Point2D(x * invScalar, y * invScalar);
    }

    Point2D<T> operator / (const Point2D & vec) const
    {
        return Point2D(x / vec.x, y / vec.y);
    }

    const Point2D<T> & operator + () const
    {
        return *this;
    }

    Point2D<T> operator - () const
    {
        return Point2D(-x, -y);
    }

    Point2D<T> & operator += (const Point2D & vec)
    {
        x += vec.x;
        y += vec.y;
        return *this;
    }
    Point2D<T> & operator += (T scalar)
    {
        x += scalar;
        y += scalar;
        return *this;
    }

    Point2D<T> & operator -= (const Point2D & vec)
    {
        x -= vec.x;
        y -= vec.y;
        return *this;
    }

    Point2D<T> & operator -= (T scalar)
    {
        x -= scalar;
        y -= scalar;
        return *this;
    }

    Point2D<T> & operator *= (T scalar)
    {
        x *= scalar;
        y *= scalar;
        return *this;
    }

    Point2D<T> & operator *= (const Point2D & vec)
    {
        x *= vec.x;
        y *= vec.y;
        return *this;
    }

    Point2D<T> & operator /= (T scalar)
    {
        // TODO: handle error scalar=0
        T invScalar = 1.0 / scalar;
        x *= invScalar;
        y *= invScalar;
        return *this;
    }

    Point2D<T> & operator /= (const Point2D & vec)
    {
        x /= vec.x;
        y /= vec.y;
        return *this;
    }
};

typedef Point2D<float> Point2Df;

#endif // POINT2D_HPP
