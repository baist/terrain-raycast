#ifndef SAMPLERBILINTERP_HPP
#define SAMPLERBILINTERP_HPP

#include <cmath>
#include "Point2D.hpp"
#include "TSurface.hpp"

template<class T>
class SamplerBilInterp
{
public:
    static Point2Df wrapCoord(const Point2Df & p)
    {
        float intX;
        float fracX = std::modf(p.x, &intX);
        if(std::signbit(p.x)) {
            fracX = 1.0f + fracX;
        }
        float intY;
        float fracY = std::modf(p.y, &intY);
        if(std::signbit(p.y)) {
            fracY = 1.0f + fracY;
        }
        return Point2Df(fracX, fracY);
    }
public:
    SamplerBilInterp(const TSurface<T> & surf):
        m_surf(surf)
    {

    }

    ~SamplerBilInterp()
    {

    }

    ColorVal getFrag(const Point2Df & p) const
    {
        Point2Df newP = wrapCoord(p);
        newP.x *= static_cast<float>(m_surf.width());
        newP.y *= static_cast<float>(m_surf.height());
        const int x0 = std::floor(newP.x);
        const int x1 = std::ceil(newP.x);
        const int y0 = std::floor(newP.y);
        const int y1 = std::ceil(newP.y);
        const float a = newP.x - std::floor(newP.x);
        const float b = newP.y - std::floor(newP.y);
        const ColorVal p0 = ColorVal(m_surf.getFrag(x0, y0));
        const ColorVal p1 = ColorVal(m_surf.getFrag(x1, y0));
        const ColorVal p2 = ColorVal(m_surf.getFrag(x0, y1));
        const ColorVal p3 = ColorVal(m_surf.getFrag(x1, y1));
        const ColorVal res = (p0 * (1.0 - a) + p1 * a) * (1.0 - b) +
                (p2 * (1.0 - a) + p3 * a) * b;
        return res;
    }
private:
    const TSurface<T> & m_surf;
};

#endif // SAMPLERBILINTERP_HPP
