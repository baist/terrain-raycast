#ifndef DRAWINGWINDOW_HPP
#define DRAWINGWINDOW_HPP

#include <memory>
#include <QSharedPointer>
#include <QWidget>
#include "Util.hpp"
#include "HeightMap.hpp"
#include "RCTerrainRender.hpp"

class DrawingWindow: public QWidget
{
public:
    DrawingWindow(QWidget * p_parent = nullptr);
    virtual ~DrawingWindow();
protected:
    virtual void paintEvent(QPaintEvent*);
private:
    std::shared_ptr<ResultSurface> m_p_textureSurf;
    std::shared_ptr<HeightMap> m_p_hMap;
    std::shared_ptr<ResultSurface> m_p_resultSurf;
    std::shared_ptr<RCTerrainRender> m_p_terrainRender;
};

#endif // DRAWINGWINDOW_HPP
