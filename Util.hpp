#ifndef UTIL_HPP
#define UTIL_HPP

#include <QImageReader>
#include <TSurface.hpp>

typedef TSurface<PixelB8G8R8X8> ResultSurface;

template<class T>
TSurface<T> fromQImageReader(QImageReader & reader);

template<>
ResultSurface fromQImageReader(QImageReader & reader);

#endif // UTIL_HPP
