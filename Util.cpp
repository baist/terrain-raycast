#include "Util.hpp"

template<class T>
TSurface<T> fromQImageReader(QImageReader & reader)
{
    QImage img = reader.read();
    if(img.isNull()) {
        return TSurface<T>(0, 0);
    }
    const unsigned int W = img.width();
    const unsigned int H = img.height();
    TSurface<T> res(W, H);
    T * p_data = reinterpret_cast<T *>(res.data());
    for (unsigned int y = 0; y < H; ++y) {
        for (unsigned int x = 0; x < W; ++x) {
            p_data[x] = img.pixelColor(x, y);
        }
        p_data += W;
    }
    return res;
}


template<>
ResultSurface fromQImageReader(QImageReader & reader)
{
    QImage img = reader.read();
    if(img.isNull()) {
        return ResultSurface(0, 0);
    }
    const unsigned int W = img.width();
    const unsigned int H = img.height();
    ResultSurface res(W, H);
    PixelB8G8R8X8 * p_data = reinterpret_cast<PixelB8G8R8X8 *>(res.data());
    for (unsigned int y = 0; y < H; ++y) {
        for (unsigned int x = 0; x < W; ++x) {
            p_data[x].r = img.pixelColor(x, y).red();
            p_data[x].g = img.pixelColor(x, y).green();
            p_data[x].b = img.pixelColor(x, y).blue();
            p_data[x].x = 0;
        }
        p_data += W;
    }
    return res;
}
