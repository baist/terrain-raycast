#ifndef SURFACE_HPP
#define SURFACE_HPP

#include "Buffer.hpp"

class Surface: public Buffer
{
public:
    Surface();
    explicit Surface(unsigned int w, unsigned int h,
        unsigned int fragmentSize);
    virtual ~Surface();
    unsigned int width() const;
    unsigned int height() const;
private:
    unsigned int m_width;
    unsigned int m_height;
};

#endif // SURFACE_HPP
