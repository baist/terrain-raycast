#-------------------------------------------------
#
# Project created by QtCreator 2018-07-10T23:31:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = terrain-raycast
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        MainWindow.cpp \
    DrawingWindow.cpp \
    HeightMap.cpp \
    DepthSurface.cpp \
    Surface.cpp \
    Buffer.cpp \
    TSurface.cpp \
    SamplerBilInterp.cpp \
    Util.cpp \
    ColorVal.cpp \
    SamplerNearInterp.cpp \
    RCTerrainRender.cpp \
    Point2D.cpp \
    Point3D.cpp

HEADERS += \
    DrawingWindow.hpp \
    HeightMap.hpp \
    DepthSurface.hpp \
    Surface.hpp \
    Buffer.hpp \
    TSurface.hpp \
    SamplerBilInterp.hpp \
    Util.hpp \
    ColorVal.hpp \
    SamplerNearInterp.hpp \
    RCTerrainRender.hpp \
    Point2D.hpp \
    Point3D.hpp \
    MainWindow.hpp

FORMS += \
        MainWindow.ui
