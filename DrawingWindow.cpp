#include "DrawingWindow.hpp"
#include <QPainter>
#include <QImageReader>

const int WIDTH = 1024;
const int HEIGHT = 1024;

DrawingWindow::DrawingWindow(QWidget * p_parent):
    QWidget (p_parent)
{
    m_p_resultSurf = std::shared_ptr<ResultSurface> (
                new ResultSurface(WIDTH, HEIGHT));
    QImageReader rdr0("./../terrain-raycast/data/C14W.png");
    ResultSurface srf0 = fromQImageReader<PixelB8G8R8X8>(rdr0);
    m_p_textureSurf = std::make_shared<ResultSurface>(srf0);
    QImageReader rdr1("./../terrain-raycast/data/D14.png");
    ResultSurface srf1 = fromQImageReader<PixelB8G8R8X8>(rdr1);
    m_p_hMap = std::shared_ptr<HeightMap> (new HeightMap(srf1));
    m_p_terrainRender = std::shared_ptr<RCTerrainRender>(
        new RCTerrainRender(m_p_textureSurf, m_p_hMap));

    // Color of sky
    m_p_resultSurf->fill(ColorVal(0.52f, 0.80f, 0.98f).asB8G8R8X8());
    m_p_terrainRender->setSurfTarget(m_p_resultSurf);
}

DrawingWindow::~DrawingWindow()
{
}

void DrawingWindow::paintEvent(QPaintEvent*)
{
    QPainter painter(this);
    //painter.setPen(QColor(255, 255, 255));

    m_p_terrainRender->render();
    QImage resultImage(reinterpret_cast<const uchar*>(
        m_p_resultSurf->constData()), m_p_resultSurf->width(),
        m_p_resultSurf->height(), QImage::Format_RGB32);
    painter.drawImage(0, 0, resultImage);

    QImage textureImage(reinterpret_cast<const uchar*>(
        m_p_textureSurf->constData()), m_p_textureSurf->width(),
        m_p_textureSurf->height(), QImage::Format_RGB32);
    painter.drawImage(QRect(resultImage.width(), 0, 512, 512), textureImage);

    /*QImage hmapImage(reinterpret_cast<const uchar*>(
        m_p_hMap->constData()), m_p_hMap->width(),
        m_p_hMap->height(), QImage::Format_RGB32);
    painter.drawImage(resultImage.width(), 0, hmapImage);*/
}
