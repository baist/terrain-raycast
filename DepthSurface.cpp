#include <algorithm>
#include "DepthSurface.hpp"

DepthSurface::DepthSurface(unsigned int w, unsigned int h):
    TSurface(w, h)
{
}

DepthSurface::~DepthSurface()
{
}

void DepthSurface::clear(float val)
{
    fill(val);
}
