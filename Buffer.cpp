#include "Buffer.hpp"

Buffer::Buffer(size_t size):
    m_size(size)
{
}

Buffer::~Buffer()
{
}

size_t Buffer::size() const
{
    return m_size;
}

void *Buffer::data()
{
    return nullptr;
}

const void * Buffer::constData() const
{
    return nullptr;
}
