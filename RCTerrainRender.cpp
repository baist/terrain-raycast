#include "RCTerrainRender.hpp"

#include "SamplerBilInterp.hpp"

RCTerrainRender::RCTerrainRender(std::shared_ptr<ResultSurface> textureSurf,
                                 std::shared_ptr<HeightMap> hMap,
                                 float xzScale, float yScale):
    m_p_textureSurf(textureSurf),
    m_p_hMap(hMap),
    m_pov(500.0f, 200.0f, 500.0f),
    m_xzScale(xzScale),
    m_yScale(yScale)
{
    m_xzScaleInv = 1.0f / m_xzScale;
    m_yScaleInv = 1.0f / m_yScale;
    m_resultWidth = m_p_textureSurf->width();
    m_resultHeight = m_p_textureSurf->height();

    m_pov.x = 0;

    m_ybuffer.resize(m_resultWidth);
    std::fill(m_ybuffer.begin(), m_ybuffer.end(), m_resultHeight);

    m_p_depthSurf = std::shared_ptr<DepthSurface>(
        new DepthSurface(m_resultWidth, m_resultHeight));
}

RCTerrainRender::~RCTerrainRender()
{
}

void RCTerrainRender::setSurfTarget(std::shared_ptr<ResultSurface> surf)
{
    m_p_resultSurf = surf;
}

void RCTerrainRender::setPOV(const Point3Df & pov)
{
    m_pov = pov;
}

void RCTerrainRender::render()
{
    // precalculate viewing angle parameters
    const float INC_Z = 0.0005f;
    const float PHI = -M_PI_2;
    const float HEIGHT = m_pov.y;
    const float HORIZON = static_cast<float>(m_resultHeight) / 4.0f;
    const float DISTANCE = 500.0f;
    const float SINPHI = std::sin(PHI);
    const float COSPHI = std::cos(PHI);
    // delta Z
    float dZ = 0.0f;
    float z = 1.0f;

    while(z < DISTANCE) {
        // Find line on map. This calculation corresponds to a field of view of 90°
        Point2Df pleft ((-COSPHI * z - SINPHI * z) + m_pov.x,
                        ( SINPHI * z - COSPHI * z) + m_pov.z);
        Point2Df pright (( COSPHI * z - SINPHI * z) + m_pov.x,
                        (-SINPHI * z - COSPHI * z) + m_pov.z);

        // segment the line
        Point2Df delta(
            (pright.x - pleft.x) / static_cast<float>(m_resultWidth),
            (pright.y - pleft.y) / static_cast<float>(m_resultWidth)
        );

        const float invZ = 1.0f / z * 240.0f;
        // Raster line and draw a vertical line for each segment
        for (unsigned int x = 0; x < m_resultWidth; ++x) {
            const unsigned int heightOnScreen =
                static_cast<unsigned int>(
                    (HEIGHT - heightByMap(pleft)) * invZ + HORIZON
                );
            drawVerticalLine(x, heightOnScreen,
                m_ybuffer[x], colorFromMap(pleft));
            if (heightOnScreen < m_ybuffer[x]) {
                drawIntoZBuffer(x, heightOnScreen,
                    m_ybuffer[x], (z - 1.0f) / DISTANCE);
                m_ybuffer[x] = heightOnScreen;
            }
            pleft += delta;
        }
        z += dZ;
        dZ += INC_Z;
    }
}

float RCTerrainRender::heightByMap(const Point2Df & p) const
{
    const HeightMap & hm = *m_p_hMap;
    SamplerBilInterp<float> smplr(hm);
    return smplr.getFrag(p * m_xzScaleInv).g * m_yScale;
}

ColorVal RCTerrainRender::colorFromMap(const Point2Df & p) const
{
    SamplerBilInterp<PixelB8G8R8X8> smplr(*m_p_textureSurf);
    return smplr.getFrag(p * m_xzScaleInv);
}

void RCTerrainRender::drawVerticalLine(unsigned int x, unsigned int ytop,
    unsigned int ybottom, const ColorVal & col)
{
    if(ytop > ybottom) {
        return;
    }
    PixelB8G8R8X8 currCol = col.asB8G8R8X8();
    PixelB8G8R8X8 * p_pixels =
        reinterpret_cast<PixelB8G8R8X8 *>(m_p_resultSurf->data());
    size_t offset = static_cast<size_t>((ytop * m_p_resultSurf->width()) + x);
    for (unsigned int k = ytop; k < ybottom; ++k) {
        p_pixels[offset].r = currCol.r;
        p_pixels[offset].g = currCol.g;
        p_pixels[offset].b = currCol.b;
        offset += m_resultWidth;
    }
}

// Is not used now but will be needed in the future
void RCTerrainRender::drawIntoZBuffer(unsigned int x, unsigned int ytop,
    unsigned int ybottom, const float z)
{
    if(ytop > ybottom) {
        return;
    }
    float * p_z = reinterpret_cast<float *>(m_p_depthSurf->data());
    size_t offset = static_cast<size_t>((ytop * m_p_depthSurf->width()) + x);
    for (unsigned int k = ytop; k < ybottom; ++k) {
        p_z[offset] = z;
        offset += m_resultWidth;
    }
}
