#include "ColorVal.hpp"

const ColorVal ColorVal::ZERO = ColorVal(0.0, 0.0, 0.0, 0.0);
const ColorVal ColorVal::ONE = ColorVal(1.0, 1.0, 1.0, 1.0);
const ColorVal ColorVal::BLACK = ColorVal(0.0, 0.0, 0.0);
const ColorVal ColorVal::WHITE = ColorVal(1.0, 1.0, 1.0);
const ColorVal ColorVal::RED = ColorVal(1.0, 0.0, 0.0);
const ColorVal ColorVal::GREEN = ColorVal(0.0, 1.0, 0.0);
const ColorVal ColorVal::BLUE = ColorVal(0.0, 0.0, 1.0);
