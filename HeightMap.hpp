#ifndef HEIGHTMAP_HPP
#define HEIGHTMAP_HPP

#include "TSurface.hpp"

class HeightMap: public TSurface<float>
{
public:
    HeightMap(const TSurface<PixelB8G8R8X8> & surf);
    HeightMap(unsigned int w, unsigned int h);
    virtual ~HeightMap();
private:
};

#endif // HEIGHTMAP_HPP
