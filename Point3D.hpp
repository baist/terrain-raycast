#ifndef POINT3D_HPP
#define POINT3D_HPP

template<class T>
class Point3D
{
public:
    T x;
    T y;
    T z;
public:
    Point3D()
    {

    }
    explicit Point3D(T _x, T _y, T _z):
        x(_x),
        y(_y),
        z(_z)
    {

    }
    ~Point3D()
    {

    }

    Point3D<T> operator + (const Point3D & rhs) const
    {
        return Point3D(x + rhs.x, y + rhs.y, z + rhs.z);
    }

    Point3D<T> operator - (const Point3D & rhs) const
    {
        return Point3D(x - rhs.x, y - rhs.y, z + rhs.z);
    }

    Point3D<T> operator * (T scalar) const
    {
        return Point3D(x * scalar, y * scalar, z * scalar);
    }

    Point3D<T> operator * (const Point3D & rhs) const
    {
        return Point3D(x * rhs.x, y * rhs.y, z * rhs.z);
    }

    Point3D<T> operator / (T scalar) const
    {
        // TODO: handle error scalar=0
        T invScalar = 1.0 / scalar;
        return Point3D(x * invScalar, y * invScalar, z * invScalar);
    }

    Point3D<T> operator / (const Point3D & rhs) const
    {
        return Point3D(x / rhs.x, y / rhs.y, z / rhs.z);
    }

    const Point3D<T> & operator + () const
    {
        return *this;
    }

    Point3D<T> operator - () const
    {
        return Point3D(-x, -y, -z);
    }

    Point3D<T> & operator += (const Point3D & rhs)
    {
        x += rhs.x;
        y += rhs.y;
        z += rhs.z;
        return *this;
    }
    Point3D<T> & operator += (T scalar)
    {
        x += scalar;
        y += scalar;
        z += scalar;
        return *this;
    }

    Point3D<T> & operator -= (const Point3D & rhs)
    {
        x -= rhs.x;
        y -= rhs.y;
        z -= rhs.z;
        return *this;
    }

    Point3D<T> & operator -= (T scalar)
    {
        x -= scalar;
        y -= scalar;
        z -= scalar;
        return *this;
    }

    Point3D<T> & operator *= (T scalar)
    {
        x *= scalar;
        y *= scalar;
        z *= scalar;
        return *this;
    }

    Point3D<T> & operator *= (const Point3D & rhs)
    {
        x *= rhs.x;
        y *= rhs.y;
        z *= rhs.y;
        return *this;
    }

    Point3D<T> & operator /= (T scalar)
    {
        // TODO: handle error scalar=0
        T invScalar = 1.0 / scalar;
        x *= invScalar;
        y *= invScalar;
        z *= invScalar;
        return *this;
    }

    Point3D<T> & operator /= (const Point3D & rhs)
    {
        x /= rhs.x;
        y /= rhs.y;
        z /= rhs.z;
        return *this;
    }
};

typedef Point3D<float> Point3Df;

#endif // POINT3D_HPP
