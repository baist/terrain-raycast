#ifndef COLORVAL_HPP
#define COLORVAL_HPP

#include <cstdint>
#include <cmath>

struct PixelR8G8B8X8
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t x;
};

struct PixelB8G8R8X8
{
    uint8_t b;
    uint8_t g;
    uint8_t r;
    uint8_t x;
};

struct PixelR8G8B8A8
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
};

struct PixelB8G8R8A8
{
    uint8_t b;
    uint8_t g;
    uint8_t r;
    uint8_t a;
};

static const float COL_INV = 1.0f / 255.0f;

class ColorVal
{
public:
    static const ColorVal ZERO;
    static const ColorVal ONE;
    static const ColorVal BLACK;
    static const ColorVal WHITE;
    static const ColorVal RED;
    static const ColorVal GREEN;
    static const ColorVal BLUE;
public:
    float r, g, b, a;
public:
    ColorVal():
        r(0.0f),
        g(0.0f),
        b(0.0f),
        a(1.0f)
    {
    }

    explicit ColorVal(float red, float green, float blue, float alpha = 1.0f):
        r(red),
        g(green),
        b(blue),
        a(alpha)
    {
    }

    explicit ColorVal(float val):
        r(val),
        g(val),
        b(val),
        a(1.0f)
    {
    }

    explicit ColorVal(const PixelR8G8B8X8 & pxl)
    {
        r = pxl.r * COL_INV;
        g = pxl.g * COL_INV;
        b = pxl.b * COL_INV;
        a = 1.0f;
    }

    explicit ColorVal(const PixelB8G8R8X8 & pxl)
    {
        r = pxl.r * COL_INV;
        g = pxl.g * COL_INV;
        b = pxl.b * COL_INV;
        a = 1.0f;
    }

    ColorVal(const ColorVal & rhs)
    {
        r = rhs.r;
        g = rhs.g;
        b = rhs.b;
        a = rhs.a;
    }

    inline bool operator == (const ColorVal &rhs) const
    {
        return (std::islessgreater(r, rhs.r) &&
                std::islessgreater(g, rhs.g) &&
                std::islessgreater(b, rhs.b));
    }

    inline bool operator!= (const ColorVal &rhs) const
    {
        return !(*this == rhs);
    }

    // arithmetic operations
    inline ColorVal operator + ( const ColorVal & rhs) const
    {
        ColorVal ret;
        ret.r = r + rhs.r;
        ret.g = g + rhs.g;
        ret.b = b + rhs.b;
        ret.a = a + rhs.a;
        return ret;
    }

    inline ColorVal operator - ( const ColorVal & rhs) const
    {
        ColorVal ret;
        ret.r = r - rhs.r;
        ret.g = g - rhs.g;
        ret.b = b - rhs.b;
        ret.a = a - rhs.a;
        return ret;
    }

    inline ColorVal operator * ( const ColorVal & rhs) const
    {
        ColorVal ret;
        ret.r = rhs.r * r;
        ret.g = rhs.g * g;
        ret.b = rhs.b * b;
        ret.a = rhs.a * a;
        return ret;
    }

    inline ColorVal operator / ( const ColorVal & rhs) const
    {
        ColorVal ret;
        ret.r = r / rhs.r;
        ret.g = g / rhs.g;
        ret.b = b / rhs.b;
        ret.a = a / rhs.a;
        return ret;
    }

    inline ColorVal operator * (const float scalar) const
    {
        ColorVal ret;
        ret.r = scalar * r;
        ret.g = scalar * g;
        ret.b = scalar * b;
        ret.a = scalar * a;
        return ret;
    }

    inline ColorVal operator / (const float scalar) const
    {
        // TODO: handle error when scalar=0
        ColorVal ret;
        const float inv = 1.0f / scalar;
        ret.r = r * inv;
        ret.g = g * inv;
        ret.b = b * inv;
        ret.a = a * inv;
        return ret;
    }

    inline void operator=(const ColorVal & rhs)
    {
        r = rhs.r;
        g = rhs.g;
        b = rhs.b;
        a = rhs.a;
    }

    // arithmetic updates
    inline ColorVal & operator += (const ColorVal & rhs)
    {
        r += rhs.r;
        g += rhs.g;
        b += rhs.b;
        a += rhs.a;
        return *this;
    }

    inline ColorVal & operator -= (const ColorVal & rhs)
    {
        r -= rhs.r;
        g -= rhs.g;
        b -= rhs.b;
        a -= rhs.a;
        return *this;
    }

    inline ColorVal & operator *= (const ColorVal & rhs)
    {
        r *= rhs.r;
        g *= rhs.g;
        b *= rhs.b;
        a *= rhs.a;
        return *this;
    }

    inline ColorVal & operator /= (const ColorVal & rhs)
    {
        r /= rhs.r;
        g /= rhs.g;
        b /= rhs.b;
        a /= rhs.a;
        return *this;
    }

    inline ColorVal & operator *= (const float scalar)
    {
        r *= scalar;
        g *= scalar;
        b *= scalar;
        a *= scalar;
        return *this;
    }

    inline ColorVal & operator /= (const float scalar)
    {
        // TODO: handle error when scalar=0
        const float inv = 1.0f / scalar;
        r *= inv;
        g *= inv;
        b *= inv;
        a *= inv;
        return *this;
    }

    inline PixelR8G8B8X8 asR8G8B8X8() const
    {
        const uint8_t intR = static_cast<uint8_t>(r * 255.0f);
        const uint8_t intG = static_cast<uint8_t>(g * 255.0f);
        const uint8_t intB = static_cast<uint8_t>(b * 255.0f);
        return PixelR8G8B8X8 {
            intR,
            intG,
            intB,
            0
        };
    }

    inline PixelR8G8B8A8 asR8G8B8A8() const
    {
        const uint8_t intR = static_cast<uint8_t>(r * 255.0f);
        const uint8_t intG = static_cast<uint8_t>(g * 255.0f);
        const uint8_t intB = static_cast<uint8_t>(b * 255.0f);
        const uint8_t intA = static_cast<uint8_t>(a * 255.0f);
        return PixelR8G8B8A8 {
            intR,
            intG,
            intB,
            intA
        };
    }

    inline PixelB8G8R8X8 asB8G8R8X8() const
    {
        const uint8_t intR = static_cast<uint8_t>(r * 255.0f);
        const uint8_t intG = static_cast<uint8_t>(g * 255.0f);
        const uint8_t intB = static_cast<uint8_t>(b * 255.0f);
        return PixelB8G8R8X8 {
            intB,
            intG,
            intR,
            0
        };
    }

    inline PixelB8G8R8A8 asB8G8R8A8() const
    {
        const uint8_t intR = static_cast<uint8_t>(r * 255.0f);
        const uint8_t intG = static_cast<uint8_t>(g * 255.0f);
        const uint8_t intB = static_cast<uint8_t>(b * 255.0f);
        const uint8_t intA = static_cast<uint8_t>(a * 255.0f);
        return PixelB8G8R8A8 {
            intB,
            intG,
            intR,
            intA
        };
    }
};

#endif // COLORVAL_HPP
