#include "Surface.hpp"

Surface::Surface():
    Buffer(0),
    m_width(0),
    m_height(0)
{

}

Surface::Surface(unsigned int w, unsigned int h, unsigned int fragmentSize):
    Buffer(w * h * fragmentSize),
    m_width(w),
    m_height(h)
{
}

Surface::~Surface()
{
}

unsigned int Surface::width() const
{
    return m_width;
}

unsigned int Surface::height() const
{
    return m_height;
}
