#include "HeightMap.hpp"

HeightMap::HeightMap(const TSurface<PixelB8G8R8X8> & surf):
    TSurface(surf.width(), surf.height())
{
    float * p_data = reinterpret_cast<float *>(data());
    const unsigned int W = surf.width();
    const unsigned int H = surf.height();
    for (unsigned int y = 0; y < H; ++y) {
        for (unsigned int x = 0; x < W; ++x) {
            const uint8_t val = surf.getFrag(x, y).g;
            p_data[x] = static_cast<float>(val) / 255.0f;
        }
        p_data += W;
    }
}

HeightMap::HeightMap(unsigned int w, unsigned int h):
    TSurface(w, h)
{

}

HeightMap::~HeightMap()
{
}
