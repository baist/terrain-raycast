#ifndef RCTERRAINRENDER_HPP
#define RCTERRAINRENDER_HPP

#include <memory>
#include "Point2D.hpp"
#include "Point3D.hpp"
#include "TSurface.hpp"
#include "DepthSurface.hpp"
#include "HeightMap.hpp"
#include "Util.hpp"

// Ray casting render for terrain
class RCTerrainRender
{
public:
    RCTerrainRender(std::shared_ptr<ResultSurface> textureSurf,
                    std::shared_ptr<HeightMap> hMap,
                    float xzScale = 1000.0f,
                    float yScale = 250.0f);
    ~RCTerrainRender();
    void setSurfTarget(std::shared_ptr<ResultSurface> surf);
    void setPOV(const Point3Df & pov);
    void render();
private:
    unsigned int m_resultWidth;
    unsigned int m_resultHeight;
    std::vector<unsigned int> m_ybuffer;
    std::shared_ptr<ResultSurface> m_p_resultSurf;
    std::shared_ptr<DepthSurface> m_p_depthSurf;
    std::shared_ptr<ResultSurface> m_p_textureSurf;
    std::shared_ptr<HeightMap> m_p_hMap;
    Point3Df m_pov;
    float m_xzScale;
    float m_yScale;
    float m_xzScaleInv;
    float m_yScaleInv;
private:
    float heightByMap(const Point2Df & p) const;
    ColorVal colorFromMap(const Point2Df & p) const;
    void drawVerticalLine(unsigned int x, unsigned int ytop,
        unsigned int ybottom, const ColorVal & col);
    void drawIntoZBuffer(unsigned int x, unsigned int ytop,
        unsigned int ybottom, const float z);
};

#endif // RCTERRAINRENDER_HPP
